-- Your SQL goes here
CREATE TABLE posts(
	post_id SERIAL PRIMARY KEY,
	title VARCHAR NOT NULL,
	link VARCHAR NOT NULL,
	author_id integer NOT NULL,
	image VARCHAR,
	CONSTRAINT fk_author 
		FOREIGN KEY(author_id) 
		REFERENCES users(user_id)
);
