-- Your SQL goes here
CREATE TABLE comments(
	comment_id SERIAL NOT NULL,
	post_id integer NOT NULL,
	author_id integer NOT NULL,
	text VARCHAR NOT NULL,
	parent_comment_id integer,
	parent_post_id integer,
	CONSTRAINT pk_comment PRIMARY KEY (comment_id, post_id),

	CONSTRAINT fk_post
		FOREIGN KEY(post_id)
		REFERENCES posts(post_id),

	CONSTRAINT fk_author
		FOREIGN KEY(author_id)
		REFERENCES users(user_id),

	CONSTRAINT fk_parent
		FOREIGN KEY(parent_comment_id, parent_post_id)
		REFERENCES comments(comment_id, post_id),

	CONSTRAINT chk_same_post CHECK
		((parent_post_id IS NULL AND parent_comment_id IS NULL)
		 OR parent_post_id=post_id)
);
