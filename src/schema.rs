table! {
    comments (comment_id, post_id) {
        comment_id -> Int4,
        post_id -> Int4,
        author_id -> Int4,
        text -> Varchar,
        parent_comment_id -> Nullable<Int4>,
        parent_post_id -> Nullable<Int4>,
    }
}

table! {
    posts (post_id) {
        post_id -> Int4,
        title -> Varchar,
        link -> Varchar,
        author_id -> Int4,
        image -> Nullable<Varchar>,
    }
}

table! {
    users (user_id) {
        user_id -> Int4,
        username -> Varchar,
        password -> Varchar,
    }
}

joinable!(comments -> posts (post_id));

allow_tables_to_appear_in_same_query!(
    comments,
    posts,
    users,
);
