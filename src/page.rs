use askama::*;

use crate::auth::{Auth, JWT};

#[derive(Template)]
#[template(path = "header.html")]
pub struct Header {
    userauth: Option<Auth<JWT>>,
}

#[derive(Template)]
#[template(source = "", ext = "html")]
pub struct Footer;

#[derive(Template)]
#[template(path = "generic.html", escape = "none")]
pub struct Page<H: Template, M: Template, F: Template> {
    header: H,
    main: M,
    footer: F,
}

pub struct PageBuilder<M: Template> {
    auth: Option<Auth<JWT>>,
    header: Option<Header>,
    footer: Option<Footer>,
    content: Option<M>,
}

impl<M: Template> PageBuilder<M> {
    pub fn new() -> Self {
        Self {
            auth: None,
            header: None,
            footer: None,
            content: None,
        }
    }

    pub fn with_auth(self, auth: Option<Auth<JWT>>) -> Self {
        Self {
            header: self.header,
            footer: self.footer,
            content: self.content,
            auth,
        }
    }

    pub fn serve(self, content: M) -> Page<Header, M, Footer> {
        let header = match self.header {
            Some(h) => h,
            None => Header {
                userauth: self.auth,
            },
        };
        let footer = Footer;

        Page {
            header,
            footer,
            main: content,
        }
    }
}
