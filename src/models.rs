use super::schema::*;
use serde::*;

#[derive(Insertable)]
#[table_name = "users"]
pub struct NewUser {
    pub username: String,
    pub password: String,
}

#[derive(Identifiable, Queryable, Insertable, Serialize, Deserialize)]
#[primary_key(user_id)]
#[table_name = "users"]
pub struct User {
    pub user_id: i32,
    pub username: String,
    pub password: String,
}

#[derive(Insertable)]
#[table_name = "posts"]
pub struct NewPost {
    pub title: String,
    pub link: String,
    pub author_id: i32,
    pub image: Option<String>,
}

#[derive(Identifiable, Queryable, Insertable, Associations, Serialize, Deserialize)]
#[belongs_to(User, foreign_key = "author_id")]
#[primary_key(post_id)]
#[table_name = "posts"]
pub struct Post {
    pub post_id: i32,
    pub title: String,
    pub link: String,
    pub author_id: i32,
    pub image: Option<String>,
}

#[derive(Queryable, Debug)]
pub struct Entry {
    pub post_id: i32,
    pub title: String,
    pub link: String,
    pub image: Option<String>,
    pub username: String,
}

#[derive(Identifiable, Queryable, Insertable, Associations, Debug, Clone)]
#[primary_key(comment_id)]
#[belongs_to(User, foreign_key = "author_id")]
#[belongs_to(Post, foreign_key = "post_id")]
#[table_name = "comments"]
pub struct Comment {
    pub comment_id: i32,
    pub post_id: i32,
    pub author_id: i32,
    pub text: String,
    pub parent_comment_id: Option<i32>,
    pub parent_post_id: Option<i32>,
}

#[derive(Insertable)]
#[table_name = "comments"]
pub struct NewComment {
    pub post_id: i32,
    pub author_id: i32,
    pub text: String,
    pub parent_comment_id: Option<i32>,
    pub parent_post_id: Option<i32>,
}
