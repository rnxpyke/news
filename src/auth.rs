use super::{PageBuilder, Secret, DB};

use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

use rocket::http::{Cookie, Cookies, Status};
use rocket::request::*;
use rocket::response::*;
use rocket::State;

extern crate hmac;
extern crate sha2;
use hmac::{Hmac, NewMac};
use jwt::{SignWithKey, VerifyWithKey};
use sha2::Sha256;

use askama::Template;
use serde::*;

use super::models;
use super::schema;
use diesel::prelude::*;
use schema::*;

#[derive(FromForm, Debug, Clone)]
pub struct Login {
    username: String,
    password: String,
}

#[derive(Template)]
#[template(path = "form/login.html")]
pub struct LoginFormTemplate;

#[derive(FromForm, Debug, Clone)]
pub struct Register {
    username: String,
    password: String,
    repassword: String,
}

#[derive(Template)]
#[template(path = "form/register.html")]
pub struct RegisterFormTemplate;

pub struct Auth<T> {
    inner: T,
}

impl<T> Deref for Auth<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T> DerefMut for Auth<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

#[derive(Serialize, Deserialize)]
pub struct JWT {
    pub id: i32,
    pub username: String,
}

#[post("/logout")]
pub fn logout(mut cookies: Cookies) -> Redirect {
    cookies.remove(Cookie::named("jwt"));
    Redirect::to("/")
}

#[get("/login")]
pub fn login_form() -> impl Responder<'static> {
    PageBuilder::new().serve(LoginFormTemplate)
}

#[post("/login", data = "<login_form>")]
pub fn login(
    login_form: Form<Login>,
    mut cookies: Cookies,
    conn: DB,
    secret_key: State<Secret>,
) -> impl Responder<'static> {
    use users::*;
    let mut res = users::table
        .order(user_id)
        .filter(username.eq(&login_form.username))
        .limit(2)
        .get_results::<models::User>(&*conn)
        .unwrap()
        .into_iter();

    let user = match res.next() {
        Some(u) => u,
        None => return Redirect::to("/login"),
    };

    if res.next().is_some() {
        panic!();
    }

    use pwhash::bcrypt;
    if !bcrypt::verify(&login_form.password, &user.password) {
        return Redirect::to("/login");
    }

    let key: Hmac<Sha256> = Hmac::new_varkey(secret_key.clone()).unwrap();
    let claims = JWT {
        id: user.user_id,
        username: user.username.clone(),
    };
    let token_str = claims.sign_with_key(&key).unwrap();
    cookies.add(Cookie::new("jwt", token_str));
    Redirect::to("/")
}

#[get("/register")]
pub fn register_form() -> impl Responder<'static> {
    PageBuilder::new().serve(RegisterFormTemplate)
}

#[post("/register", data = "<register_form>")]
pub fn register(
    register_form: Form<Register>,
    mut cookies: Cookies,
    conn: DB,
    secret_key: State<Secret>,
) -> Redirect {
    let register = register_form;

    use schema::users::*;
    let mut res = users::table
        .filter(username.eq(&register.username))
        .limit(1)
        .get_results::<models::User>(&*conn)
        .unwrap()
        .into_iter();

    if res.next().is_some() {
        return Redirect::to("/register");
    }

    if register.password != register.repassword {
        return Redirect::to("/register");
    }

    use pwhash::bcrypt;
    let new = models::NewUser {
        username: register.username.clone(),
        password: bcrypt::hash(&register.password).unwrap(),
    };

    let inserted = diesel::insert_into(users::table)
        .values(new)
        .get_results::<models::User>(&*conn)
        .unwrap();

    let key: Hmac<Sha256> = Hmac::new_varkey(secret_key.clone()).unwrap();
    let claims = JWT {
        id: inserted[0].user_id,
        username: inserted[0].username.clone(),
    };
    let token_str = claims.sign_with_key(&key).unwrap();
    cookies.add(Cookie::new("jwt", token_str));
    Redirect::to("/")
}

#[derive(Debug)]
pub enum AuthError {
    Missing,
    Invalid,
    Internal,
}

pub struct Token<T> {
    value: String,
    inner: PhantomData<T>,
}

#[derive(Debug)]
pub enum TokenError {
    Missing,
}

impl<'a, 'r> FromRequest<'a, 'r> for Token<JWT> {
    type Error = TokenError;
    fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        use rocket::outcome::Outcome::*;
        let cookies = match Cookies::from_request(request) {
            Failure((s, _)) => return Failure((s, TokenError::Missing)),
            Forward(()) => return Forward(()),
            Success(cookies) => cookies,
        };

        let token = match cookies.get("jwt").map(|c| c.value()) {
            Some(jwt) => jwt,
            _ => return Failure((Status::Unauthorized, TokenError::Missing)),
        };

        Success(Token {
            value: token.into(),
            inner: PhantomData,
        })
    }
}

impl<'a, 'r, T> FromRequest<'a, 'r> for Auth<T>
where
    Token<T>: FromRequest<'a, 'r>,
    for<'de> T: Deserialize<'de>,
{
    type Error = AuthError;
    fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        use rocket::outcome::Outcome::*;
        use AuthError::*;

        let key = match request
            .guard::<State<Secret>>()
            .map_failure(|(s, _)| (s, Internal))
            .and_then(|secret| {
                Hmac::<Sha256>::new_varkey(&secret)
                    .map(Success)
                    .unwrap_or(Failure((Status::InternalServerError, Internal)))
            }) {
            Success(key) => key,
            Forward(()) => return Forward(()),
            Failure(e) => return Failure(e),
        };

        let token = match request.guard::<Token<T>>() {
            Success(v) => v,
            Forward(()) => return Forward(()),
            Failure((s, _)) => return Failure((s, Missing)),
        };

        let inner: T = match token.value.verify_with_key(&key) {
            Ok(res) => res,
            _ => return Failure((Status::Unauthorized, Invalid)),
        };
        Success(Auth { inner })
    }
}
