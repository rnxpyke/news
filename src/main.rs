#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use]
extern crate rocket;

#[macro_use]
extern crate rocket_contrib;

#[macro_use]
extern crate diesel;

use rocket::request::*;
use rocket::response::*;
use rocket_contrib::json::Json;
use rocket_contrib::serve::StaticFiles;

use askama::Template;

mod auth;
mod models;
mod page;
mod schema;

use auth::{Auth, JWT};
use page::*;

use diesel::prelude::*;
use models::{Entry, Post};
use schema::*;

use petgraph::prelude::*;
use std::collections::BTreeMap;
use std::error::Error;

#[derive(Template)]
#[template(path = "posts.html")]
struct PostTemplate {
    posts: Vec<Entry>,
    page: u32,
}

#[derive(FromForm)]
struct CreatePost {
    title: String,
    link: String,
}

fn is_base_url(link: &str) -> std::result::Result<(), Box<dyn std::error::Error>> {
    use url::*;
    let url = Url::parse(link)?;
    if url.cannot_be_a_base() {
        let e: Box<dyn Error> = "url can't be a base url".into();
        Err(e)?
    }
    if url.scheme() == "javascript" {
        let e: Box<dyn Error> = "url scheme can't contain javascript".into();
        Err(e)?
    }
    Ok(())
}

fn get_preview_image(url: &str) -> Option<String> {
    opengraph::scrape(url, Default::default()).ok()
        .and_then(|o| o.images
            .into_iter()
            .map(|x| x.url)
            .next())
}

#[post("/createpost", data = "<postdata>")]
fn create(conn: DB, user: Auth<JWT>, postdata: Form<CreatePost>) -> std::result::Result<Redirect, Box<dyn std::error::Error>> {
    let mut new_post = models::NewPost {
        title: postdata.title.clone(),
        link: postdata.link.clone(),
        author_id: user.id,
        image: None,
    };

    is_base_url(&postdata.link)?;

    new_post.image = get_preview_image(&postdata.link);

    diesel::insert_into(posts::table)
        .values(new_post)
        .get_results::<Post>(&*conn)?;

    Ok(Redirect::to("/"))
}

#[derive(Template)]
#[template(path = "form/createpost.html")]
struct CreatePostTemplate;

#[get("/createpost")]
fn createpost(auth: Option<Auth<JWT>>) -> impl Responder<'static> {
    PageBuilder::new().with_auth(auth).serve(CreatePostTemplate)
}

#[get("/posts")]
fn posts(conn: DB) -> impl Responder<'static> {
    use diesel::prelude::*;
    use schema::posts;
    posts::table.load::<models::Post>(&*conn).map(Json)
}

#[get("/?<page>")]
fn ranking(conn: DB, auth: Option<Auth<JWT>>, page: Option<u32>) -> impl Responder<'static> {
    use posts::*;
    use users::*;

    const PAGE_SIZE: i64 = 24;

    posts::table
        .inner_join(users::table.on(users::user_id.eq(posts::author_id)))
        .select((post_id, title, link, image, username))
        .limit(PAGE_SIZE)
        .offset(page.unwrap_or(0) as i64 * PAGE_SIZE)
        .order(post_id.desc())
        .get_results::<models::Entry>(&*conn)
        .map(|posts| {
            PageBuilder::new()
                .with_auth(auth)
                .serve(PostTemplate { posts, page: page.unwrap_or(0) })
        })
}

#[derive(Template)]
#[template(path = "form/reply-to.html")]
struct ReplyTemplate {
    post_id: String,
    comment_id: Option<String>,
}

#[get("/comment/<post>/<comment>")]
fn reply_to(post: String, comment: Option<String>, auth: Option<Auth<JWT>>) -> impl Responder<'static> {
    PageBuilder::new()
        .with_auth(auth)
        .serve(ReplyTemplate {
            post_id: post,
            comment_id: comment
        })
}


#[database("postgres")]
pub struct DB(diesel::PgConnection);

#[derive(FromForm, Debug)]
struct CreateComment {
    pub post_id: i32,
    pub text: String,
    pub parent_comment_id: Option<i32>,
}

#[post("/comment", data = "<form>")]
fn comment(conn: DB, user: Auth<JWT>, form: Form<CreateComment>) -> impl Responder<'static> {
    let CreateComment {
        post_id,
        text,
        parent_comment_id,
    } = form.into_inner();
    let new = models::NewComment {
        post_id,
        text,
        parent_comment_id,
        parent_post_id: parent_comment_id.map(|_| post_id),
        author_id: user.id,
    };
    diesel::insert_into(comments::table)
        .values(new)
        .get_results::<models::Comment>(&*conn)
        .map(|_| ())
}



#[derive(Debug, Copy, Clone)]
pub enum Nest<T> {
    In,
    Out,
    Node(T),
}

impl<T> std::ops::Deref for Nest<T> {
    type Target = T;
    #[track_caller]
    fn deref(&self) -> &T {
        match self {
            Nest::Node(val) => &val,
            _ => panic!("called Nest::deref()  on a none value"),
        }
    }
}

impl<T> Nest<T> {
    #[track_caller]
    pub fn unwrap(self) -> T {
        match self {
            Nest::Node(val) => val,
            _ => panic!("called Nest::unwrap() on non `Node` value"),
        }
    }

    pub fn into_inner(self) -> Option<T> {
        match self {
            Nest::Node(val) => Some(val),
            _ => None,
        }

    }

    pub fn is_in(&self) -> bool {
        match self {
            Nest::In => true,
            _ => false
        }
    }

    pub fn is_out(&self) -> bool {
        match self {
            Nest::Out => true,
            _ => false
        }
    }

    pub fn is_node(&self) -> bool {
        match self {
            Nest::Node(_) => true,
            _ => false
        }
    }



}

#[derive(Debug, Copy, Clone)]
struct Depth<T> {
    depth: i32,
    value: T,
}


impl<T> std::iter::FromIterator<Depth<T>> for Vec<Nest<T>> {
    fn from_iter<I: IntoIterator<Item=Depth<T>>>(iter: I) -> Self {
        let mut res = Vec::new();
        let mut depth = 0;
        for item in iter {
            let mut diff = item.depth - depth;
            while diff != 0 {
                if diff > 0 {
                    res.push(Nest::In);
                    diff -= 1;
                } else {
                    res.push(Nest::Out);
                    diff += 1;
                }
            }
            res.push(Nest::Node(item.value));
            depth = item.depth;
        }

        res
    }
}

#[derive(Template)]
#[template(path = "post-details.html")]
struct DetailsTemplate {
    post: Post,
    comments: Vec<Nest<models::Comment>>
}

#[get("/post/<id>")]
fn post_details(
    conn: DB,
    auth: Option<Auth<JWT>>,
    id: i32,
) -> QueryResult<impl Responder<'static>> {
    let post: Post = posts::table.find(id).first(&*conn)?;
    let comments: BTreeMap<_, _> = comments::table
        .filter(comments::post_id.eq(id))
        .get_results::<models::Comment>(&*conn)
        .unwrap()
        .into_iter()
        .map(|c| (c.comment_id, c))
        .collect();

    let mut graph: GraphMap<_, _, Directed> = GraphMap::new();
    for c in comments.values() {
        if let Some(parent) = c.parent_comment_id {
            let id = c.comment_id;
            graph.add_edge(parent, id, 1);
        } else {
            graph.add_node(c.comment_id);
        }
    }

    let mut depth = 0;
    let mut nodes: Vec<Depth<i32>> = graph
        .nodes()
        .filter(|&n| graph.neighbors_directed(n, Incoming).next().is_none())
        .map(|value| Depth { depth, value })
        .collect();

    for _ in 0..comments.len() {
        use std::iter::once;
        let length = nodes.len();

        nodes = nodes
            .iter()
            .map(|&n| {
                if n.depth == depth {
                    let children = graph
                        .neighbors_directed(n.value, Outgoing)
                        .map(|value| Depth {
                            depth: depth + 1,
                            value,
                        });
                    let iter = once(n).chain(children);
                    Box::new(iter) as Box<dyn Iterator<Item = Depth<i32>>>
                } else {
                    Box::new(once(n)) as Box<dyn Iterator<Item = Depth<i32>>>
                }
            }).flatten()
            .collect();

        depth += 1;
        if nodes.len() <= length {
            break;
        }
    }

    let tree: Vec<Nest<_>> = nodes.into_iter().collect();
    let comments = tree.into_iter().map(|nest|
        match nest {
            Nest::In => Nest::In,
            Nest::Out => Nest::Out,
            Nest::Node(n) => Nest::Node(comments[&n].clone())
        }).collect();

    Ok(PageBuilder::new()
        .with_auth(auth)
        .serve(DetailsTemplate { post, comments }))
}

pub type Secret = &'static [u8];

fn main() {
    let secret: &'static [u8] = b"some-secret";

    rocket::ignite()
        .attach(DB::fairing())
        .mount("/", routes![auth::login, auth::login_form, auth::logout])
        .mount("/", routes![auth::register, auth::register_form])
        .mount(
            "/",
            routes![ranking, post_details, create, createpost, comment, reply_to],
        ).mount("/api", routes![posts])
        .mount(
            "/",
            StaticFiles::from(concat!(env!("CARGO_MANIFEST_DIR"), "/static")),
        ).manage(secret)
        .launch();
}
